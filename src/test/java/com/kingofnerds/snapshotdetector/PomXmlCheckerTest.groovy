package com.kingofnerds.snapshotdetector

import org.junit.Assert
import org.junit.Test
import org.springframework.test.util.ReflectionTestUtils
import org.springframework.web.client.RestTemplate

class PomXmlCheckerTest {
    @Test
    public void testPomXmlForHubCommon() {
        def restTemplate = new RestTemplate()
        def pomXmlChecker = new PomXmlChecker()
        ReflectionTestUtils.setField(pomXmlChecker, "restTemplate", restTemplate)

        def containsSnapshot = pomXmlChecker.containsSnapshotDependency("https://github.com/blackducksoftware/hub-common")
        Assert.assertFalse(containsSnapshot)
    }

    @Test
    public void testPomXmlForHubMaven() {
        def restTemplate = new RestTemplate()
        def pomXmlChecker = new PomXmlChecker()
        ReflectionTestUtils.setField(pomXmlChecker, "restTemplate", restTemplate)

        def containsSnapshot = pomXmlChecker.containsSnapshotDependency("https://github.com/blackducksoftware/hub-maven-plugin")
        Assert.assertTrue(containsSnapshot)
    }
}
