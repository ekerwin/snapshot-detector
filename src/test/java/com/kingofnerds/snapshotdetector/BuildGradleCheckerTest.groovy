package com.kingofnerds.snapshotdetector

import org.junit.Assert
import org.junit.Test
import org.springframework.test.util.ReflectionTestUtils
import org.springframework.web.client.RestTemplate

class BuildGradleCheckerTest {
    @Test
    public void testBuildGradleForEmailExtension() {
        def restTemplate = new RestTemplate()
        def buildGradleChecker = new BuildGradleChecker()
        ReflectionTestUtils.setField(buildGradleChecker, "restTemplate", restTemplate)

        def containsSnapshot = buildGradleChecker.containsSnapshotDependency("https://github.com/blackducksoftware/hub-email-extension")
        Assert.assertTrue(containsSnapshot)
    }

    @Test
    public void testBuildGradleForHubGradle() {
        def restTemplate = new RestTemplate()
        def buildGradleChecker = new BuildGradleChecker()
        ReflectionTestUtils.setField(buildGradleChecker, "restTemplate", restTemplate)

        def containsSnapshot = buildGradleChecker.containsSnapshotDependency("https://github.com/blackducksoftware/hub-gradle-plugin")
        Assert.assertTrue(containsSnapshot)
    }
}
