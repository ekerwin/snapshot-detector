package com.kingofnerds.snapshotdetector

import org.apache.commons.lang3.StringUtils
import org.jsoup.Jsoup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class PomXmlChecker {
    @Autowired
    final RestTemplate restTemplate

    def boolean containsSnapshotDependency(String url) {
        def pomXmlUri = new URI("${url}/raw/master/pom.xml")
        def pomXmlContents = restTemplate.getForObject(pomXmlUri, String.class)
        if (StringUtils.isBlank(pomXmlContents)) {
            println "couldn't find pom.xml for ${url}"
            return false
        }

        def document = Jsoup.parse(pomXmlContents)
        def projectVersion = document.select("project>version").first().text()

        def snapshotCount = 0
        snapshotCount += StringUtils.countMatches(projectVersion, "SNAPSHOT")
        snapshotCount += StringUtils.countMatches(pomXmlContents, "SNAPSHOT")

        //the only legal location for SNAPSHOT is in project>version, which we double count
        snapshotCount == 1 || snapshotCount > 2
    }
}
