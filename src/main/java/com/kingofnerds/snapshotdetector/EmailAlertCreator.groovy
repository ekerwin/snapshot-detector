package com.kingofnerds.snapshotdetector

import javax.mail.MessagingException
import javax.mail.internet.MimeMessage

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Component

@Component
class EmailAlertCreator {
    @Value('${email.to.addresses}')
    def toAddresses

    @Autowired
    final JavaMailSender javaMailSender

    def send(String contents) {
        final MimeMessage mail = javaMailSender.createMimeMessage()
        try {
            final MimeMessageHelper helper = new MimeMessageHelper(mail)
            toAddresses.tokenize(',').each { helper.addTo(it) }
            helper.setFrom("giggles@blackducksoftware.com")
            helper.setSubject("HOLY SHIT...SNAPSHOTS!!!")
            helper.setText(contents)
        } catch (final MessagingException e) {
            e.printStackTrace()
        }
        javaMailSender.send(mail)
    }
}
