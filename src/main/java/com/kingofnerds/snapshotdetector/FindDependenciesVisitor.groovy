package com.kingofnerds.snapshotdetector

import org.codehaus.groovy.ast.CodeVisitorSupport
import org.codehaus.groovy.ast.expr.ArgumentListExpression
import org.codehaus.groovy.ast.expr.Expression
import org.codehaus.groovy.ast.expr.MapExpression
import org.codehaus.groovy.ast.expr.MethodCallExpression

class FindDependenciesVisitor extends CodeVisitorSupport {
    def dependenciesFound = false
    int dependenciesFirstLine
    int dependenciesLastLine

    public void visitMethodCallExpression(MethodCallExpression methodCallExpression) {
        if (methodCallExpression.getMethodAsString().equals("dependencies")) {
            if (!dependenciesFound) {
                dependenciesFirstLine = methodCallExpression.lineNumber
                dependenciesLastLine = methodCallExpression.lastLineNumber
            }
        }

        super.visitMethodCallExpression(methodCallExpression)
    }

    public void visitArgumentlistExpression(ArgumentListExpression argumentListExpression) {
        List<Expression> expressions = argumentListExpression.expressions
        super.visitArgumentlistExpression(argumentListExpression)
    }

    public void visitMapExpression(MapExpression mapExpression) {
        super.visitMapExpression(mapExpression)
    }
}