package com.kingofnerds.snapshotdetector

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class BuildGradleChecker {
    @Autowired
    final RestTemplate restTemplate

    def boolean containsSnapshotDependency(String url) {
        def buildGradleUri = new URI("${url}/raw/master/build.gradle")
        def buildGradleContents = restTemplate.getForObject(buildGradleUri, String.class)
        if (StringUtils.isBlank(buildGradleContents)) {
            println "couldn't find build.gradle for ${url}"
            return false
        }

        def snapshotCount = StringUtils.countMatches(buildGradleContents, "SNAPSHOT")
        return snapshotCount > 1
    }
}
