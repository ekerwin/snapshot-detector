package com.kingofnerds.snapshotdetector

import javax.annotation.PostConstruct

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class RepositoryChecker {
    def static final pomRepositories = [
        "hub-common":"https://github.com/blackducksoftware/hub-common",
        "hub-maven-plugin":"https://github.com/blackducksoftware/hub-maven-plugin",
        "hub-bamboo":"https://github.com/blackducksoftware/hub-bamboo",
        "hub-jira":"https://github.com/blackducksoftware/hub-jira",
        "hub-teamcity":"https://github.com/blackducksoftware/hub-teamcity",
        "hub-jenkins":"https://github.com/jenkinsci/blackduck-hub-plugin",
        "phone-home-api":"https://github.com/blackducksoftware/phone-home-api",
        "integration-common":"https://github.com/blackducksoftware/integration-common",
        "hub-atlassian-config":"https://github.com/blackducksoftware/hub-atlassian-config",
        "hub-eclipse-plugin":"https://github.com/blackducksoftware/hub-eclipse-plugin",
        "common-maven-parent":"https://github.com/blackducksoftware/common-maven-parent"
    ]
    def static final gradleRepositories = [
        "hub-gradle-plugin":"https://github.com/blackducksoftware/hub-gradle-plugin",
        "hub-email-extension":"https://github.com/blackducksoftware/hub-email-extension"
    ]

    @Autowired EmailAlertCreator emailAlertCreator
    @Autowired PomXmlChecker pomXmlChecker
    @Autowired BuildGradleChecker buildGradleChecker

    @Scheduled(cron = '${cron.expression}')
    @PostConstruct
    def checkRepositories() {
        StringBuilder stringBuilder = new StringBuilder()
        pomRepositories.each { projectName, url ->
            if (pomXmlChecker.containsSnapshotDependency(url)) {
                stringBuilder.append("${projectName} has a SNAPSHOT dependency!\n")
            }
        }

        gradleRepositories.each { projectName, url ->
            if (buildGradleChecker.containsSnapshotDependency(url)) {
                stringBuilder.append("${projectName} has a SNAPSHOT dependency!\n")
            }
        }

        if (stringBuilder.length() > 0) {
            emailAlertCreator.send(stringBuilder.toString())
        }
    }
}
